#ifndef UTIL_FILE_H
#define UTIL_FILE_H

#include <stdio.h>

enum file_status{
	OPEN_OK=0,
	OPEN_ERROR,
	OPEN_READ_ERROR,
	OPEN_WRITE_ERROR
};

static const char* file_status_string[]={
	[OPEN_OK]="FILE OPEN: OK",
	[OPEN_ERROR]="FILE OPEN: NOT OK",
	[OPEN_READ_ERROR]="FILE OPEN: READ ERROR",
	[OPEN_WRITE_ERROR]="FILE OPEN: WRITE ERROR"
};

enum file_status open_file_for_reading(const char* file_name, FILE** file);
enum file_status open_file_for_writing(const char* file_name, FILE** file);

#endif
