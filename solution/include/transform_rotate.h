#ifndef TRANSFORM_ROTATE_H
#define TRANSFORM_ROTATE_H

#include "internal.h"
#include <stdint.h>

enum rotation_status{
	ROTATE_OK=0,
	ROTATE_ERROR,
	ROTATE_INVALID
};enum rotation_status rotation(struct image** img);

static const char* rotation_status_string[]={
	[ROTATE_OK]="ROTATE: OK",
	[ROTATE_ERROR]="ROTATE: ERROR",
	[ROTATE_INVALID]="ROTATE: INVALID ANGLE"
};

#endif
