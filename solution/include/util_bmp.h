#ifndef UTIL_BMP_H
#define UTIL_BMP_H

#include "internal.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)


enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_FILE
};enum read_status from_bmp(FILE* in, struct image** img);

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};enum write_status to_bmp(FILE* out, struct image const* img);


static const char* read_status_string[]={
    [READ_OK]="READ: OK",
    [READ_INVALID_FILE]="READ: ERROR - BMP FILE BROKEN",
    [READ_INVALID_BITS]="READ: ERROR - BMP FILE IS NOT 24 BITS",
    [READ_INVALID_HEADER]="READ: ERROR - BMP HEADER INVALID",
    [READ_INVALID_SIGNATURE]="READ: ERROR - BMP FILE INVALID"
};

static const char* write_status_string[]={
    [WRITE_OK]="WRITE: OK",
    [WRITE_ERROR]="WRITE: NOT OK"
};

#endif
