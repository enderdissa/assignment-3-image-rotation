#ifndef INTERNAL_H
#define INTERNAL_H

#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct pixel {
    int8_t r, g, b;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image* init(const uint64_t width, const uint64_t height);

void destruct_image(struct image* img);

#endif
