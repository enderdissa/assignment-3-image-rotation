#include "transform_rotate.h"
//transformation(rotate) module
enum rotation_status rotation(struct image** img){
	uint32_t image_w = (*img)->width;
	uint32_t image_h = (*img)->height;
	struct image* rotated_image = init(image_h, image_w);
	if (rotated_image==NULL){
		return ROTATE_ERROR;
	}
	for (size_t i=0; i<image_w;i++){
		for (size_t j=0; j< image_h; j++){
			rotated_image->data[image_h * i+j]=(*img)->data[image_w * (image_h-j-1)+i];
		}
	}destruct_image(*img);
	(*img)=rotated_image;
	return ROTATE_OK;
}
