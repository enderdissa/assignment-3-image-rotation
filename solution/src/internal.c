#include "internal.h"
//internal module
struct image* init(const uint64_t width, const uint64_t height){
	struct image* img=malloc(sizeof(struct image));
	if (img == NULL) {
		return NULL;
	}
	struct pixel* data_pix=malloc(sizeof(struct pixel) * width * height);
	if (data_pix == NULL) {
		free(img);
		return NULL;
	}
	img->width= width;
	img->height= height;
	img->data= data_pix;
	return img;
}

void destruct_image(struct image* img){
	if (img){
		if (img->data){
			free(img->data);
		}free(img);
	}
}

