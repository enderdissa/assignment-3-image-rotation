#include "util_file.h"
//file utility module
enum file_status open_file_for_reading(const char* file_name, FILE** file){
	*file = fopen(file_name,"rb");
	if (!*file){
		return OPEN_READ_ERROR;
	}
	return OPEN_OK;
}
enum file_status open_file_for_writing(const char* file_name, FILE** file){
	*file = fopen(file_name,"wb");
	if (!*file){
		return OPEN_WRITE_ERROR;
	}else{return OPEN_OK;}
}
