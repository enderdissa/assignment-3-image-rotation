#include "util_bmp.h"
#include "internal.h"
#include "transform_rotate.h"
#include "util_bmp.h"
#include "util_file.h"
#include <stdio.h>
//main module
int main( int argc, char **argv ) {
    //(void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc!=4){
    	fprintf(stderr,"wrong arguments. follow the format.");
        return -1;
    }

    const char* file_read_name = argv[1];
    const char* file_write_name = argv[2];
    int32_t angle = (int32_t) atoi(argv[3]);

    FILE* file_read = NULL;
    FILE* file_write=NULL;
    struct image* img = NULL;

    enum file_status file_read_status = open_file_for_reading(file_read_name, &file_read);
    if (file_read_status){
    	fprintf(stderr, "%s",file_status_string[file_read_status]);
    	return file_read_status;
    }

    enum read_status bmp_read=from_bmp(file_read,&img);
    if (bmp_read){
        fprintf(stderr, "%s", read_status_string[bmp_read]);
        destruct_image(img);
        return bmp_read;
    }fclose(file_read);
    //rotation
    enum rotation_status img_rotate;
    if (angle%90){
        fprintf(stderr, "%s", rotation_status_string[ROTATE_INVALID]);
    }
    if (angle<90){
        angle+=360;
    }
    while (angle){
        img_rotate = rotation(&img);
        if (img_rotate){
            fprintf(stderr,"%s",rotation_status_string[img_rotate]);
            destruct_image(img);
            return img_rotate;
        }
        angle-=90;
    }
    
    enum file_status file_open_status = open_file_for_writing(file_write_name, &file_write);
    if (file_open_status){
        fprintf(stderr, "%s", file_status_string[file_open_status]);
        destruct_image(img);
        return file_open_status;
    }

    enum write_status write_bmp_status = to_bmp(file_write, img);
    if (write_bmp_status){
        fprintf(stderr,"%s",write_status_string[write_bmp_status]);
        destruct_image(img);
        return write_bmp_status;
    }
    fclose(file_write);
    destruct_image(img);
    return 0;
}
