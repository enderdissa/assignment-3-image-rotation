#include "util_bmp.h"
//bmp reader and writer module
static enum read_status read_bmp_header(FILE* file, struct bmp_header* header){
	if(fread(header, sizeof(struct bmp_header), 1, file)!=1){
		return READ_INVALID_HEADER;
	}return READ_OK;
}
//
    static uint8_t getPadding(uint32_t w) {
        return ((4 - (w * sizeof(struct pixel)) % 4) % 4);
    }
static struct bmp_header bmp_header_new(const struct image* img) {
    uint32_t w = img->width;
    uint32_t h = img->height;
    uint8_t p = getPadding(w);
        return (struct bmp_header) {
            .bfType = 0x4D42,
            .bfileSize = sizeof(struct bmp_header) +
                         (w + p) * h * sizeof(struct pixel),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = w,
            .biHeight = h,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = (w + p) * h * sizeof(struct pixel),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

}


    enum read_status from_bmp(FILE* in, struct image** img) {
        struct bmp_header header = {0};

        if (read_bmp_header(in, &header)) {
            return READ_INVALID_HEADER;
        }
        if (header.biBitCount != 24) {
            return READ_INVALID_BITS;
        }
        if (header.bfType != 0x4D42) {
            return READ_INVALID_SIGNATURE;
        }

        uint32_t w = header.biWidth;
        uint32_t h = header.biHeight;
        uint8_t p = getPadding(w);
        
        *img = init(w, h);
        if(!(*img)->data){
            return READ_INVALID_FILE;
        }
        fseek(in, header.bOffBits, SEEK_SET);
        for (size_t i = 0; i < h; i++) {
            if (fread(&((*img)->data[(h-1-i) * w]),sizeof(struct pixel), w, in)!=w){
                return READ_INVALID_FILE;
            }
            fseek(in, (long)p, SEEK_CUR);
        }
        return READ_OK;
    }

    enum write_status to_bmp(FILE* out, const struct image* img) {
        struct bmp_header header = bmp_header_new(img);
        uint32_t w = img->width;
        uint32_t h = img->height;
        uint8_t p = getPadding(w);
        uint8_t zero[4]={0};
        // char *out_img = malloc(header.biSizeImage);
        if (fwrite(&header, sizeof(struct bmp_header), 1, out)!=1){
            return WRITE_ERROR;
        }
        fseek(out, header.bOffBits, SEEK_SET);
        for (size_t i=0; i<h;i++){
            if (fwrite(&(img->data[(h-1-i) * w]),
                sizeof(struct pixel),w,out)!=w){
                return WRITE_ERROR;
            }
            if (fwrite(zero, sizeof(uint8_t), p, out)!=p) {
            return WRITE_ERROR;
        }        
    }
    return WRITE_OK;
}
